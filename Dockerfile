FROM node:14.19.1-alpine3.14 AS builder
RUN mkdir /ng-app
WORKDIR /ng-app
COPY . .
RUN npm ci
RUN npm run build -- ---output-path=dist

FROM nginx:1.13.3-alpine
COPY nginx/default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*
COPY build/ /usr/share/nginx/html/
CMD ["nginx", "-g", "daemon off;"]
